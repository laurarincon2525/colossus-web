import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { InicioComponent } from './inicio/inicio.component';
import { TiposerviciosComponent } from './tiposervicios/tiposervicios.component';
import { SugerenciaComponent } from './sugerencia/sugerencia.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { SuscribirseComponent } from './suscribirse/suscribirse.component';
import { AppRoutingModule } from './app-routing.module';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {
  MatNativeDateModule,
  MatBottomSheetModule,
  MAT_BOTTOM_SHEET_DEFAULT_OPTIONS,
  MatCardModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { ConocenosComponent } from './conocenos/conocenos.component';
import { PromocionesComponent } from './promociones/promociones.component';
import { MatTabsModule, MatSidenavModule } from '@angular/material';
import { MatListModule } from '@angular/material';
import { SidenavListComponent } from './sidenav-list/sidenav-list.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CatalogoComponent, BottomSheetOverviewExampleSheetComponent } from './catalogo/catalogo.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatRadioModule } from '@angular/material/radio';
import { FilterPipePipe } from './pipes/filter-pipe.pipe';
import { PreguntasfrecuentesComponent } from './preguntasfrecuentes/preguntasfrecuentes.component';
import { SlideshowModule } from 'ng-simple-slideshow';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavbarComponent,
    InicioComponent,
    TiposerviciosComponent,
    SugerenciaComponent,
    SuscribirseComponent,
    NosotrosComponent,
    ConocenosComponent,
    PromocionesComponent,
    SidenavListComponent,
    CatalogoComponent,
    FilterPipePipe,
    PreguntasfrecuentesComponent,
    BottomSheetOverviewExampleSheetComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    AppRoutingModule,
    MatStepperModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatSidenavModule,
    MatListModule,
    FlexLayoutModule,
    MatExpansionModule,
    MatMenuModule,
    HttpClientModule,
    MatRadioModule,
    MatBottomSheetModule,
    SlideshowModule,
    MatCardModule,
  ],
  entryComponents: [
    BottomSheetOverviewExampleSheetComponent,
  ],
  providers: [
    MatDatepickerModule,
    MatToolbarModule,
    { provide: MAT_BOTTOM_SHEET_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
