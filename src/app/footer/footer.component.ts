import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  providers: [ServicesService]
})
export class FooterComponent implements OnInit {
  empresa: any;
  horario: any;
  url: any;
  constructor(private serv: ServicesService) { }

  ngOnInit() {
    this.serv.obtenerEmpresa().subscribe(resp => {
      console.log(resp);
      this.empresa = resp.data;
    });

    this.serv.obtenerHorarioTrabajo().subscribe(resp => {
      console.log(resp);
      this.horario = resp.data;
    });

  }

  abririnsta(url) {
    this.url = url.instagram;
    console.log(this.url);
    window.open(this.url, '_blank');
  }

  abrirtwit(url) {
    this.url = url.twitter;
    console.log(this.url);
    window.open(this.url, '_blank');
  }

  abrirface(url) {
    this.url = url.facebook;
    console.log(this.url);
    window.open(this.url, '_blank');
  }

}
