import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ServicesService } from '../services/services.service';
import * as M from '../../assets/js/materialize.min.js';
import * as $ from 'jquery';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
  providers: [ServicesService]
})
export class InicioComponent implements OnInit, AfterViewInit {

  marcas: any;
  caracteristicas: any;
  descarga: any;
  empresa: any;
  carrusel = [];
  options = { duration: 1000, fullwidth: true };
  imageSources = [];
  constructor(private serv: ServicesService) {
  }

  ngOnInit() {
    this.serv.obtenerMarca().subscribe(resp => {
      this.marcas = resp.data;
    });

    this.serv.obteneCaracteristicasEmpresa().subscribe(resp => {
      console.log(resp);
      this.caracteristicas = resp.data;
      // console.log(this.marcas[0].nombre);
    });

    this.serv.obtenerDescargaApp().subscribe(resp => {
      console.log(resp);
      this.descarga = resp.data;
      // console.log(this.marcas[0].nombre);
    });

    this.serv.obtenerEmpresa().subscribe(resp => {
      console.log(resp);
      this.empresa = resp.data;
    });

    const elems = document.querySelectorAll('.carousel');
    const instances = M.Carousel.init(elems, this.options);

  }

  ngAfterViewInit() {

  this.serv.obtenerCarrusel().subscribe(resp => {
    console.log(resp);
    this.carrusel = resp.data;
    this.carrusel.forEach((element: any) => {
      this.imageSources.push(element.url_imagen);
    });
    console.log(this.carrusel[0].url_imagen);
  });
  }
}
