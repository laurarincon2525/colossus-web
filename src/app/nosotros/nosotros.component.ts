import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services/services.service';
import * as $ from 'jquery';
@Component({
  selector: 'app-nosotros',
  templateUrl: './nosotros.component.html',
  styleUrls: ['./nosotros.component.css'],
  providers: [ServicesService]
})
export class NosotrosComponent implements OnInit {
nosotros: any;
nosotros2:[];

  constructor(private serv: ServicesService) { }

  ngOnInit() {
    this.serv.obtenerEmpresa().subscribe( resp => {

      this.nosotros2 = resp.data
  })

  console.log(this.nosotros2)
}
}
