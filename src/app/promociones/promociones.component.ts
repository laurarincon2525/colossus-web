import { Component, OnInit } from '@angular/core';
import {ServicesService} from '../services/services.service';

@Component({
  selector: 'app-promociones',
  templateUrl: './promociones.component.html',
  styleUrls: ['./promociones.component.css'],
  providers: [ServicesService]
})
export class PromocionesComponent implements OnInit {
  promociones: any[] = [];
  textFilter: any;
  filterPromocion: any[] = [];

  constructor(private serv: ServicesService) { }

  ngOnInit() {
    this.serv.obtenerPromociones().subscribe( resp => {
      console.log(resp);
      this.promociones = resp.data;
      this.filterPromocion= this.promociones;
  });
}

filtrarPromocion() {
  //texto buscado en minusculas
  let tx = this.textFilter.toLowerCase();
  // expresion regular para encontrar concidencias en el array
  let regex = new RegExp("\(\^" + tx + "\)+", 'm');
  if (tx === '') {
    this.filterPromocion = this.promociones;
  } else {
    //Filtra el array de acuerdo al texto buscado
    this.filterPromocion = this.promociones.filter((item) => {
      if (
        regex.exec(item.nombre.toLowerCase()) ||
        regex.exec(item.catalogoServicio.descripcion.toLowerCase()) ||
        regex.exec(item.descuento.nombre.toLowerCase())
      ) {
        return true;
      }
    })
  }
}

}
