import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposerviciosComponent } from './tiposervicios.component';

describe('TiposerviciosComponent', () => {
  let component: TiposerviciosComponent;
  let fixture: ComponentFixture<TiposerviciosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiposerviciosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposerviciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
